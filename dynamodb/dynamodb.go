package dynamodb

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	awsDynamodb "github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"log"
)

// Struct internal
type Service struct {
	Db *awsDynamodb.DynamoDB
}

// OpenSession - Open new session of DynamoDB
func (service *Service) OpenSession() (err error) {
	// Initialize a session in us-west-2 that the SDK will use to load
	// credentials from the shared credentials file ~/.aws/credentials.
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-east-1")},
	)

	if err != nil {
		log.Println("Error open session: ", err.Error())
		return  err
	}

	// Create DynamoDB client
	service.Db = awsDynamodb.New(sess)

	return  nil
}

// CreateTable - create a new table in DynamoDB - https://docs.aws.amazon.com/pt_br/sdk-for-go/v1/developer-guide/dynamo-example-create-table.html
func (service *Service) CreateTable(table *awsDynamodb.CreateTableInput) (err error) {

	_, err = service.Db.CreateTable(table)

	if err != nil {
		log.Println("Got error calling CreateTable: ", err.Error())
		return err
	}

	log.Println("Created the table in us-west-2", table.TableName)

	return nil
}

// GetAll - returns all results - https://github.com/awsdocs/aws-doc-sdk-examples/blob/master/go/example_code/dynamodb/scan_items.go
func (service *Service) GetAll(table string) (*awsDynamodb.ScanOutput, error) {

	// Build the query input parameters
	params := &awsDynamodb.ScanInput{
		TableName: aws.String(table),
	}

	// Make the DynamoDB Query API call
	result, err := service.Db.Scan(params)

	if err != nil {
		return nil, err
	}

	return result, nil
}

// Save - insert a new value in table - https://github.com/awsdocs/aws-doc-sdk-examples/blob/master/go/example_code/dynamodb/create_item.go
func (service *Service) Save(table string, columns interface{}) error {

	av, err := dynamodbattribute.MarshalMap(columns)

	if err != nil {
		fmt.Println("Got error marshalling map: ", err.Error())
		return err
	}

	// Create item in table Movies
	input := &awsDynamodb.PutItemInput{
		Item: av,
		TableName: aws.String(table),
	}

	_, err = service.Db.PutItem(input)

	if err != nil {
		log.Println("Got error calling PutItem: ", err.Error())
		return err
	}

	return nil
}

// Remove - remove register in table - https://github.com/awsdocs/aws-doc-sdk-examples/blob/master/go/example_code/dynamodb/delete_item.go
func (service *Service) Remove(condition *awsDynamodb.DeleteItemInput) error {


	_, err := service.Db.DeleteItem(condition)

	if err != nil {
		log.Println("Got error calling PutItem: ", err.Error())
		return err
	}

	log.Println("Deleted register with success.")

	return nil
}

// Update - update register in table, with new values - https://github.com/awsdocs/aws-doc-sdk-examples/blob/master/go/example_code/dynamodb/update_item.go
func (service *Service) Update(condition *awsDynamodb.UpdateItemInput) error {

	_, err := service.Db.UpdateItem(condition)

	if err != nil {
		return err
	}

	fmt.Println("Successfully updated.")

	return nil
}

// Find - find registers
func (service *Service) Find(condition *awsDynamodb.GetItemInput) (*awsDynamodb.GetItemOutput, error) {

	result, err := service.Db.GetItem(condition)

	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	return result, nil
}
