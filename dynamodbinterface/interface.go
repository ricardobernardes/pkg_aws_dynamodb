package dynamodbinterface

import awsDynamodb "github.com/aws/aws-sdk-go/service/dynamodb"

type DynamodbAPI interface {
	OpenSession() (newSession *awsDynamodb.DynamoDB, err error)
	CreateTable(table *awsDynamodb.CreateTableInput) (err error)
	GetAll(table string) (*awsDynamodb.ScanOutput, error)
	Save(table string, columns interface{}) error
	Remove(condition *awsDynamodb.DeleteItemInput) error
	Update(condition *awsDynamodb.UpdateItemInput) error
	Find(condition *awsDynamodb.GetItemInput) (*awsDynamodb.GetItemOutput, error)
}
